﻿using Fahrenheit.CoreLib;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;

namespace ffx_eevee;

public sealed record evModuleConfig : FhModuleConfig {
	public evModuleConfig(string configName, u32 configVersion, bool configEnabled)
	: base(configName, configVersion, configEnabled) { }

	public override bool TrySpawnModule([NotNullWhen(true)] out FhModule? fm) {
		fm = new evModule(this);
		return fm.ModuleState == FhModuleState.InitSuccess;
	}

}

public partial class evModule : FhModule {
	private readonly evModuleConfig _moduleConfig;

	public override FhModuleConfig ModuleConfiguration => _moduleConfig;

	public evModule(evModuleConfig cfg) : base(cfg) {
		_moduleConfig = cfg;

		createHooks();

		_moduleState = FhModuleState.InitSuccess;
	}

	public override bool FhModuleInit() {
		return true;
	}

	public override unsafe bool FhModuleStart() {
		*(FhXGlobals.tidus_limit_times + 0) = 6f; // Spiral Cut
		*(FhXGlobals.tidus_limit_times + 1) = 6f; // Slice'n'Dice
		*(FhXGlobals.tidus_limit_times + 2) = 6f; // Energy Rain
		*(FhXGlobals.tidus_limit_times + 3) = 4f; // Blitz Ace

		/*Task.Run(() => {
			FhLog.Log(LogLevel.Info, $"Awaiting value @ {FhXGlobals.cur_atel_worker + 0xC4 + 0x4:X8}");
			new FhPointer(new [] { new FhPointerDeref(FhXGlobals.cur_atel_worker + 0xC4 + 0x4, false) }).AwaitValue(0x15C55);
			FhLog.Log(LogLevel.Info, "Found the thingy!!!");
		});*/

		return applyHooks();
	}

	public override bool FhModuleStop() {
		return removeHooks();
	}

	public override bool FhModuleOnError() {
		return true;
	}
}