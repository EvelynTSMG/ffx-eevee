﻿using Fahrenheit.CLRHost;
using static ffx_eevee.evDelegates;
using static ffx_eevee.Hooks.evHooks;
using static Fahrenheit.Hooks.Generic.FhXDelegates;

namespace ffx_eevee;

public partial class evModule {
	public static FhMethodHandle<MsApUp> _evMsApUp;
	public static FhMethodHandle<MsCalcEscape> _evMsCalcEscape;
	public static FhMethodHandle<FUN_0078a420_multDmgWithElemDelegate> _evMultDmgWithElem;
	public static FhMethodHandle<MsCalcDamageDelegate> _evMsCalcDamage;
	public static FhMethodHandle<MsSetRikkuLimitDelegate> _evMsSetRikkuLimit;
	public static FhMethodHandle<FUN_0078e680Delegate> _evFUN_0078e680;
	public static FhMethodHandle<AtelExecCallFuncDelegate> _evCSR;
	public static FhMethodHandle<Sg_MainLoopDelegate> _evQuickReset;
	public static FhMethodHandle<AtelResultCallFuncDelegate> _evOwo;
	public static FhMethodHandle<showAutoSavingPromptDelegate> _evSkipAutoSavePrompt;

	private unsafe void createHooks() {
		_evMsApUp = new FhMethodHandle<MsApUpDelegate>(this, 0x398A10, eeveeMsApUp);
		_evMsCalcEscape = new FhMethodHandle<MsCalcEscapeDelegate>(this, 0x38A840, eeveeMsCalcEscape);
		_evMultDmgWithElem = new FhMethodHandle<FUN_0078a420_multDmgWithElemDelegate>(this, 0x38A420, eeveeMultDmgWithElem);
		_evMsCalcDamage = new FhMethodHandle<MsCalcDamageDelegate>(this, 0x389CB0, eeveeMsCalcDamage);
		_evMsSetRikkuLimit = new FhMethodHandle<MsSetRikkuLimitDelegate>(this, 0x3B0530, eeveeMsSetRikkuLimit);
		_evFUN_0078e680 = new FhMethodHandle<FUN_0078e680Delegate>(this, 0x38E680, eeveeFUN_0078e680);
		_evCSR = new FhMethodHandle<AtelExecCallFuncDelegate>(this, 0x4776B0, eeveeCSR);
		_evQuickReset = new FhMethodHandle<Sg_MainLoopDelegate>(this, 0x420C00, eeveeQuickReset);
		_evOwo = new FhMethodHandle<AtelResultCallFuncDelegate>(this, 0x477770, eeveeAtelResultCallFunc);
		_evSkipAutoSavePrompt = new FhMethodHandle<showAutoSavingPromptDelegate>(this, 0x26F610, eeveeSkipAutoSavePrompt);
	}

	private bool applyHooks() {
		return _evMsApUp.ApplyHook()
			&& _evMsCalcEscape.ApplyHook()
			&& _evMultDmgWithElem.ApplyHook()
			&& _evMsCalcDamage.ApplyHook()
			&& _evMsSetRikkuLimit.ApplyHook()
			&& _evFUN_0078e680.ApplyHook()
			&& _evCSR.ApplyHook()
			&& _evQuickReset.ApplyHook()
			&& _evOwo.ApplyHook()
			&& _evSkipAutoSavePrompt.ApplyHook()
			;
	}

	private bool removeHooks() {
		return _evMsApUp.RemoveHook()
			&& _evMsCalcEscape.RemoveHook()
			&& _evMultDmgWithElem.RemoveHook()
			&& _evMsCalcDamage.RemoveHook()
			&& _evMsSetRikkuLimit.RemoveHook()
			&& _evFUN_0078e680.RemoveHook()
			&& _evCSR.RemoveHook()
			&& _evQuickReset.RemoveHook()
			&& _evOwo.RemoveHook()
			&& _evSkipAutoSavePrompt.RemoveHook()
			;
	}
}
