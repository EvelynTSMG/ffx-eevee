﻿using Fahrenheit.CoreLib;
using System;
using System.IO;
using System.Linq;
using System.Text;

using static ffx_eevee.evFuncLib;

namespace ffx_eevee;

public static class evUtils {
	public class Vector4f {
		public float x = 0,
					 y = 0,
					 z = 0,
					 w = 0;
	}

	public class Matrix4f {
		public Vector4f col0 = new Vector4f(),
						col1 = new Vector4f(),
						col2 = new Vector4f(),
						col3 = new Vector4f();
	}

	public static unsafe u32 countBits(u32 n) {
		// http://graphics.stanford.edu/~seander/bithacks.html#CountBitsSetParallel
		n -= (n >> 1) & 0x55555555;
		n = (n & 0x33333333) + ((n >> 2) & 0x33333333);

		return ((n + (n >> 4) & 0x0F0F0F0F) * 0x01010101) >> 24;
	}

	public static unsafe string chrIdToName(u32 chr_id) {
		if (chr_id >= 0x14) {
			return $"Monster {chr_id - 0x14}";
		}

		u8* def_name = getDefaultChrName(chr_id);
		u8[] name = new u8[14];

		// Who hurt C# so much it made Array.Length an integer
		for (i32 i = 0; i < 14; i++) {
			if (*(def_name + i) == 0x00) {
				Array.Resize(ref name, i);
				break;
			}
			name[i] = *(def_name + i);
		}

		return Encoding.ASCII.GetString(decodeToEnglish(name));
	}

	public static unsafe u8[] evGetExcelData(string filepath, i32 idx) {

		using (var fs = File.Open(filepath, FileMode.Open, FileAccess.Read, FileShare.Read)) {
			isize file_ptr = fs.SafeFileHandle.DangerousGetHandle();

			u16 min_idx = *(u16*)(file_ptr + 0x8);
			u16 max_idx = *(u16*)(file_ptr + 0xA);
			u16 block_size = *(u16*)(file_ptr + 0xC);

			u32 data_start_offset = *(u32*)(file_ptr + 0x10);

			if (idx < min_idx || idx > max_idx) throw new IndexOutOfRangeException();

			u8[] data = new u8[block_size];

			for (i32 i = 0; i < block_size; i++) {
				data[i] = *(u8*)(file_ptr + data_start_offset + idx * block_size + i);
			}

			return data;
		}

		throw new Exception();
	}

	public static u8 decodeToEnglish(u8 b) {
		return b switch {
			0x3A => (u8)' ',
			0x3B => (u8)'!',
			0x3C => (u8)'"',
			0x3D => (u8)'#',
			0x3E => (u8)'$',
			0x3F => (u8)'%',
			0x40 => (u8)'&',
			0x41 => (u8)'\'',
			0x42 => (u8)'(',
			0x43 => (u8)')',
			0x44 => (u8)'*',
			0x45 => (u8)'+',
			0x46 => (u8)',',
			0x47 => (u8)'-',
			0x48 => (u8)'.',
			0x49 => (u8)'/',
			0x4A => (u8)':',
			0x4B => (u8)';',
			0x4C => (u8)'<',
			0x4D => (u8)'=',
			0x4E => (u8)'>',
			0x4F => (u8)'?',
			0x50 => (u8)'A',
			0x51 => (u8)'B',
			0x52 => (u8)'C',
			0x53 => (u8)'D',
			0x54 => (u8)'E',
			0x55 => (u8)'F',
			0x56 => (u8)'G',
			0x57 => (u8)'H',
			0x58 => (u8)'I',
			0x59 => (u8)'J',
			0x5A => (u8)'K',
			0x5B => (u8)'L',
			0x5C => (u8)'M',
			0x5D => (u8)'N',
			0x5E => (u8)'O',
			0x5F => (u8)'P',
			0x60 => (u8)'Q',
			0x61 => (u8)'R',
			0x62 => (u8)'S',
			0x63 => (u8)'T',
			0x64 => (u8)'U',
			0x65 => (u8)'V',
			0x66 => (u8)'W',
			0x67 => (u8)'X',
			0x68 => (u8)'Y',
			0x69 => (u8)'Z',
			0x6A => (u8)'[',
			0x6B => (u8)'\\',
			0x6C => (u8)']',
			0x6D => (u8)'^',
			0x6E => (u8)'_',
			0x6F => (u8)'\'',
			0x70 => (u8)'a',
			0x71 => (u8)'b',
			0x72 => (u8)'c',
			0x73 => (u8)'d',
			0x74 => (u8)'e',
			0x75 => (u8)'f',
			0x76 => (u8)'g',
			0x77 => (u8)'h',
			0x78 => (u8)'i',
			0x79 => (u8)'j',
			0x7A => (u8)'k',
			0x7B => (u8)'l',
			0x7C => (u8)'m',
			0x7D => (u8)'n',
			0x7E => (u8)'o',
			0x7F => (u8)'p',
			0x80 => (u8)'q',
			0x81 => (u8)'r',
			0x82 => (u8)'s',
			0x83 => (u8)'t',
			0x84 => (u8)'u',
			0x85 => (u8)'v',
			0x86 => (u8)'w',
			0x87 => (u8)'x',
			0x88 => (u8)'y',
			0x89 => (u8)'z',
			0x8A => (u8)'{',
			0x8B => (u8)'|',
			0x8C => (u8)'}',
			0x8D => (u8)'~',
			0x8E => (u8)'·',
			_ => b,
		};
	}

	public static u8[] decodeToEnglish(u8[] str) {
		// This should really use some part of DEdit but I don't wanna bother figuring it out rn

		u8[] new_str = new u8[str.Length];

		for (i32 i = 0; i < str.Length; i++) {
			new_str[i] = decodeToEnglish(str[i]);
		}

		return new_str;
	}
}
