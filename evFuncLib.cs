﻿using Fahrenheit.CoreLib;
using System;
using System.Runtime.InteropServices;
using static ffx_eevee.evDelegates;
using static ffx_eevee.evUtils;

namespace ffx_eevee;

internal static class evFuncLib {
	public static u32 MsCheckRange(u32 n, u32 min, u32 max) {
		var fn_ptr = Marshal.GetDelegateForFunctionPointer<MsCheckRangeDelegate>(new IntPtr(FhXGlobals.game_base + 0x39A0D0));
		return fn_ptr(n, min, max);
	}

	public static unsafe FhXChr* MsGetChr(u32 chr_id) {
		var fn_ptr = Marshal.GetDelegateForFunctionPointer<MsGetChrDelegate>(new IntPtr(FhXGlobals.game_base + 0x394030));
		return fn_ptr(chr_id);
	}

	public static isize MsSaveAddInfoData(u32 chr_id, isize param_2) {
		var fn_ptr = Marshal.GetDelegateForFunctionPointer<MsSaveAddInfoDataDelegate>(new IntPtr(FhXGlobals.game_base + 0x385AC0));
		return fn_ptr(chr_id, param_2);
	}

	public static i32 MsSaveItemUse(u32 item_id, i32 amount) {
		var fn_ptr = Marshal.GetDelegateForFunctionPointer<MsSaveItemUseDelegate>(new isize(FhXGlobals.game_base + 0x3905A0));
		return fn_ptr(item_id, amount);
	}

	public static unsafe FhXMemCom* MsGetRomItem(u32 item_id, isize param_2) {
		var fn_ptr = Marshal.GetDelegateForFunctionPointer<MsGetRomItemDelegate>(new IntPtr(FhXGlobals.game_base + 0x390A40));
		return fn_ptr(item_id, param_2);
	}

	public static isize FUN_007996e0(u32 mon_id, u32 com_id, isize param_3) {
		var fn_ptr = Marshal.GetDelegateForFunctionPointer<FUN_007996e0Delegate>(new IntPtr(FhXGlobals.game_base + 0x3996E0));
		return fn_ptr(mon_id, com_id, param_3);
	}

	public static isize FUN_00796a00(u32 chr_id, isize param_2) {
		var fn_ptr = Marshal.GetDelegateForFunctionPointer<FUN_00796a00Delegate>(new IntPtr(FhXGlobals.game_base + 0x396a00));
		return fn_ptr(chr_id, param_2);
	}

	public static isize MsSetSaveCommand(u32 chr_id, u32 com_id, isize param_3) {
		var fn_ptr = Marshal.GetDelegateForFunctionPointer<MsSetSaveCommandDelegate>(new IntPtr(FhXGlobals.game_base + 0x385D10));
		return fn_ptr(chr_id, com_id, param_3);
	}

	public static unsafe i32 AtelPopStackInteger(usize* work, usize* stack_ptr) {
		var fn_ptr = Marshal.GetDelegateForFunctionPointer<AtelPopStackIntegerDelegate>(new IntPtr(FhXGlobals.game_base + 0x46DE90));
		return fn_ptr(work, stack_ptr);
	}

	public static isize FUN_00860a30() {
		var fn_ptr = Marshal.GetDelegateForFunctionPointer<FUN_00860a30Delegate>(new IntPtr(FhXGlobals.game_base + 0x460a30));
		return fn_ptr();
	}

	public static void Sg_MenuSetReserve(isize param_1) {
		var fn_ptr = Marshal.GetDelegateForFunctionPointer<Sg_MenuSetReserveDelegate>(new IntPtr(FhXGlobals.game_base + 0x421870));
		fn_ptr(param_1);
	}

	public static unsafe u8* getDefaultChrName(u32 param_1) {
		var fn_ptr = Marshal.GetDelegateForFunctionPointer<getDefaultChrNameDelegate>(new IntPtr(FhXGlobals.game_base + 0x3873E0));
		return fn_ptr(param_1);
	}

	public static unsafe FhXChr* MsGetMon(u8 mon_id) {
		var fn_ptr = Marshal.GetDelegateForFunctionPointer<MsGetMonDelegate>(new IntPtr(FhXGlobals.game_base + 0x395AB0));
		return fn_ptr(mon_id);
	}

	public static u32 getChrRngIdx(u32 chr_id, u32 type) {
		var fn_ptr = Marshal.GetDelegateForFunctionPointer<getChrRngIdxDelegate>(new IntPtr(FhXGlobals.game_base + 0x38D2D0));
		return fn_ptr(chr_id, type);
	}

	public static u32 brnd(u32 rng_seed_idx) {
		var fn_ptr = Marshal.GetDelegateForFunctionPointer<brndDelegate>(new IntPtr(FhXGlobals.game_base + 0x398900));
		return fn_ptr(rng_seed_idx);
	}

	public static void AtelJumpGameOver() {
		var fn_ptr = Marshal.GetDelegateForFunctionPointer<AtelJumpGameOverDelegate>(new IntPtr(FhXGlobals.game_base + 0x46D9A0));
		fn_ptr();
	}

	public static void FUN_007c8650(isize param_1) {
		var fn_ptr = Marshal.GetDelegateForFunctionPointer<FUN_007c8650Delegate>(new IntPtr(FhXGlobals.game_base + 0x3C8650));
		fn_ptr(param_1);
	}

	public static unsafe char* TOGetShapTextureName(u32 tex_id) {
		var fn_ptr = Marshal.GetDelegateForFunctionPointer<TOGetShapTextureNameDelegate>(new IntPtr(FhXGlobals.game_base + 0x4AC870));
		return fn_ptr(tex_id);
	}

	public static unsafe void graphicDrawUIElement(char* tex_path, i32 param_2, float* param_3, char* map_path, i32 param_5, u32 param_6, f32 param_7) {
		var fn_ptr = Marshal.GetDelegateForFunctionPointer<graphicDrawUIElementDelegate>(new IntPtr(FhXGlobals.game_base + 0x23F090));
		fn_ptr(tex_path, param_2, param_3, map_path, param_5, param_6, param_7);

	}
}

