﻿using System.Linq;
using System;
using static ffx_eevee.evUtils;
using Fahrenheit.CoreLib;

namespace ffx_eevee.Systems;

public static class evMixSystem {
	public enum ItemAffinity {
		Heal,
		Esuna,
		StatUp,
		Buff,
		Debuff,
		Health,
		Mana,
		Special,
		Damage,
		Fire,
		Lightning,
		Water,
		Ice
	}

	private class ItemAffinities {
		private const string MIX_BIN_PATH = ".\\data\\FFX_Data\\ffx_ps2\\ffx\\master\\jppc\\battle\\kernel\\mix.bin";
		public i8 heal, esuna, stat_up, buff, debuff, health, mana, special, dmg, fire, lightning, water, ice;

		public ItemAffinities() { }

		public ItemAffinities(ItemAffinities affinity) {
			heal = affinity.heal;
			esuna = affinity.esuna;
			stat_up = affinity.stat_up;
			buff = affinity.buff;
			debuff = affinity.debuff;
			health = affinity.health;
			mana = affinity.mana;
			special = affinity.special;
			dmg = affinity.dmg;
			fire = affinity.fire;
			lightning = affinity.lightning;
			water = affinity.water;
			ice = affinity.ice;
		}

		public ItemAffinities(u16 item_id) : this((i8[])(Array)evGetExcelData(MIX_BIN_PATH, item_id & 0xFFF)) { }

		public ItemAffinities(i8[] affinities) {
			if (affinities.Length != 13) {
				throw new ArgumentException();
			}

			heal = affinities[0];
			esuna = affinities[1];
			stat_up = affinities[2];
			buff = affinities[3];
			debuff = affinities[4];
			health = affinities[5];
			mana = affinities[6];
			special = affinities[7];
			dmg = affinities[8];
			fire = affinities[9];
			lightning = affinities[10];
			water = affinities[11];
			ice = affinities[12];
		}

		public bool hasElement() {
			ItemAffinities temp = new ItemAffinities(this);
			temp.collapseElements();
			return fire > 0 || lightning > 0 || water > 0 || ice > 0;
		}

		public ItemAffinity[] getDominance() {
			(i8, ItemAffinity)[] values = { (heal, ItemAffinity.Heal), (esuna, ItemAffinity.Esuna), (stat_up, ItemAffinity.StatUp),
				(buff, ItemAffinity.Buff), (debuff, ItemAffinity.Debuff), (health, ItemAffinity.Health), (mana, ItemAffinity.Mana),
				(special, ItemAffinity.Special), (dmg, ItemAffinity.Damage) };
			values = values.OrderByDescending(e => e.Item1).ToArray();
			return Array.ConvertAll(values, e => e.Item2);
		}

		public ItemAffinity collapseElements() {
			i8[] elems = new[] { fire, lightning, water, ice };
			i8 max = elems.Max();
			ItemAffinity dom_elem = (ItemAffinity)(elems.ToList().IndexOf(max) + (i32)ItemAffinity.Fire);

			while ((elems[0] > 0 && elems[3] > 0) || (elems[1] > 0 && elems[2] > 0)) {
				i8 min = elems.Where(e => e != 0).Min();
				for (i32 i = 0; i < elems.Length; i++) {
					if (elems[i] > 0) {
						elems[i] -= min;
					}
				}
			}

			i8[] remains = elems.Where(e => e != 0).ToArray();

			if (remains.Length == 0) {
				fire = 0;
				lightning = 0;
				water = 0;
				ice = 0;
				return dom_elem;
			}

			if (remains.Length > 1) {
				i8 min = remains.Min();
				dmg += min;
				for (i32 i = 0; i < remains.Length; i++) {
					remains[i] -= min;
				}
			}

			fire = 0;
			lightning = 0;
			water = 0;
			ice = 0;

			if (remains.Max() != 0) {
				i8 final_dom = remains.Max();

				switch (dom_elem) {
					case ItemAffinity.Fire:
						fire = final_dom; break;
					case ItemAffinity.Lightning:
						lightning = final_dom; break;
					case ItemAffinity.Water:
						water = final_dom; break;
					case ItemAffinity.Ice:
						ice = final_dom; break;
				}
			}

			return dom_elem;
		}

		public override string ToString() {
			return $"{heal,4} | {esuna,4} | {stat_up,4} | {buff,4} | {debuff,4} | {health,4} | {mana,4} | {special,4} | {dmg,4} | {fire,4} | {lightning,4} | {water,4} | {ice,4}";
		}

		public static ItemAffinities operator +(ItemAffinities a, ItemAffinities b) {
			ItemAffinities aff = new ItemAffinities();
			aff.heal = (i8)Math.Clamp(a.heal + b.heal, -128, 127);
			aff.esuna = (i8)Math.Clamp(a.esuna + b.esuna, -128, 127);
			aff.stat_up = (i8)Math.Clamp(a.stat_up + b.stat_up, -128, 127);
			aff.buff = (i8)Math.Clamp(a.buff + b.buff, -128, 127);
			aff.debuff = (i8)Math.Clamp(a.debuff + b.debuff, -128, 127);
			aff.health = (i8)Math.Clamp(a.health + b.health, -128, 127);
			aff.mana = (i8)Math.Clamp(a.mana + b.mana, -128, 127);
			aff.special = (i8)Math.Clamp(a.special + b.special, -128, 127);
			aff.dmg = (i8)Math.Clamp(a.dmg + b.dmg, -128, 127);
			aff.fire = (i8)Math.Clamp(a.fire + b.fire, -128, 127);
			aff.lightning = (i8)Math.Clamp(a.lightning + b.lightning, -128, 127);
			aff.water = (i8)Math.Clamp(a.water + b.water, -128, 127);
			aff.ice = (i8)Math.Clamp(a.ice + b.ice, -128, 127);

			return aff;
		}
	}

	public static u16 getMixResult(u16 item1, u16 item2) {
		ItemAffinities aff1 = new ItemAffinities(item1);
		ItemAffinities aff2 = new ItemAffinities(item2);

		if (item2 == item1) {
			if (aff2.heal > 0) aff2.heal -= 1;
			if (aff2.esuna > 0) aff2.esuna -= 1;
			if (aff2.stat_up > 0) aff2.stat_up -= 1;
			if (aff2.buff > 0) aff2.buff -= 1;
			if (aff2.debuff > 0) aff2.debuff -= 1;
			if (aff2.health > 0) aff2.health -= 1;
			if (aff2.mana > 0) aff2.mana -= 1;
			if (aff2.special > 0) aff2.special -= 1;
			if (aff2.dmg > 0) aff2.dmg -= 1;
			if (aff2.fire > 0) aff2.fire -= 1;
			if (aff2.lightning > 0) aff2.lightning -= 1;
			if (aff2.water > 0) aff2.water -= 1;
			if (aff2.ice > 0) aff2.ice -= 1;
			FhLog.Log(LogLevel.Info, "Mix: Same Item Penalty Applied");
		}

		ItemAffinities total_aff = aff1 + aff2;

		ItemAffinity[] dominance = total_aff.getDominance();

		for (i32 i = 0; i < dominance.Length; i++) {
			switch (dominance[i]) {
				case ItemAffinity.Heal: {
					if (total_aff.heal <= 0) break;

					// Do we also want to remedy?
					if (total_aff.esuna > 0) {
						if (total_aff.heal >= 4) return 0x30B4; // Final Elixir
						if (total_aff.heal >= 3) return 0x30B3; // Super Elixir
						if (total_aff.heal >= 1) return 0x30AE; // Ultra Cure
					}

					if (total_aff.health > 0) {
						if (total_aff.heal >= 3) return 0x30B0; // Final Phoenix
						if (total_aff.heal >= 2) return 0x30AF; // Mega Phoenix
					}

					if (total_aff.heal >= 3) return 0x30B2; // Megalixir
					if (total_aff.heal >= 2) return 0x30B1; // Elixir
					return 0x30AC; // Ultra Potion
				}

				case ItemAffinity.Esuna:
					if (total_aff.esuna <= 0) break;

					if (total_aff.heal >= 4) return 0x30B4; // Final Elixir
					if (total_aff.heal >= 3) return 0x30B3; // Super Elixir
					if (total_aff.heal >= 1) return 0x30AE; // Ultra Cure

					return 0x30AD; // Panacea (only mix with no healing and esuna affinity)

				case ItemAffinity.StatUp: {
					if (total_aff.stat_up <= 0) break;

					if (total_aff.buff >= 2) {
						if (total_aff.hasElement()) {
							if (total_aff.stat_up >= 4) return 0x30B8; // Ultra NulAll
							if (total_aff.stat_up >= 2) return 0x30B7; // Hyper NulAll
						}
					}

					break;
				}

				case ItemAffinity.Buff: {
					if (total_aff.buff <= 0) break;

					if (total_aff.hasElement()) { // NulAlls
						if (total_aff.buff >= 2) {
							if (total_aff.stat_up >= 4 && total_aff.buff >= 2) return 0x30B8; // Ultra NulAll
							if (total_aff.stat_up >= 2 && total_aff.buff >= 2) return 0x30B7; // Hyper NulAll
							return 0x30B6; // Mega NulAll
						}

						return 0x30B5; // NulAll
					}

					if (total_aff.health >= 1 && total_aff.mana >= 1) {
						if (total_aff.buff >= 5) return 0x30BC; // Hyper Mighty G
						if (total_aff.buff >= 4) return 0x30BB; // Super Mighty G
					}

					if (total_aff.buff >= 3) return 0x30BA; // Mighty G
					if (total_aff.buff >= 2) return 0x30B9; // Mighty Wall
					break;
				}

				case ItemAffinity.Debuff:
					break;

				case ItemAffinity.Health: {
					if (total_aff.health <= 0) break;

					if (total_aff.health >= 2 && total_aff.stat_up >= 1) return 0x30BF; // Hyper Vitality
					if (total_aff.health >= 2) return 0x30BE; // Mega Vitality
					return 0x30BD; // Vitality
				}

				case ItemAffinity.Mana: {
					if (total_aff.mana <= 0) break;

					if (total_aff.buff > 0) {
						if (total_aff.mana >= 2) return 0x30C4; // Freedom X
						return 0x30C3; // Freedom
					}

					if (total_aff.mana >= 2 && total_aff.stat_up >= 1) return 0x30C2; // Hyper Mana
					if (total_aff.mana >= 2) return 0x30C1; // Mega Mana
					return 0x30C0; // Mana
				}

				case ItemAffinity.Special: {
					if (total_aff.special <= 0) break;

					if (total_aff.special >= 3) {
						if (total_aff.special >= 4 && total_aff.dmg >= 3) return 0x30C6; // Trio of 9999
						if (total_aff.dmg >= 2) return 0x30C5; // Quartet of 9
						if (total_aff.buff >= 2) return 0x30CA; // Eccentrick
						if (total_aff.dmg >= 1) return 0x30C8; // Miracle Drink
					}

					if (total_aff.special >= 2) {
						if (total_aff.buff >= 1) return 0x30C9; // Hot Spurs
						if (total_aff.dmg >= 1) return 0x30C7; // Hero Drink
					}

					break;
				}

				case ItemAffinity.Damage: {
					if (total_aff.dmg <= 0) return 0x308B; // Grenade

					ItemAffinity dom_elem = total_aff.collapseElements();

					if (total_aff.dmg >= 6) {
						if (total_aff.hasElement()) {
							switch (dom_elem) {
								case ItemAffinity.Fire:
									if (total_aff.fire >= 4) return 0x3097; // Burning Soul
									break;
								case ItemAffinity.Lightning:
									if (total_aff.lightning >= 4) return 0x30A1; // Lightning Bolt
									break;
								case ItemAffinity.Water:
									if (total_aff.water >= 4) return 0x30A6; // Tidal Wave
									break;
								case ItemAffinity.Ice:
									if (total_aff.ice >= 4) return 0x309C; // Winter Storm
									break;
							}
						}

						if (total_aff.special >= 3) return 0x30AB; // Sunburst)
					}

					if (total_aff.dmg >= 5) {
						if (total_aff.hasElement()) {
							switch (dom_elem) {
								case ItemAffinity.Fire:
									if (total_aff.fire >= 3) return 0x3097; // Burning Soul
									break;
								case ItemAffinity.Lightning:
									if (total_aff.lightning >= 3) return 0x30A1; // Lightning Bolt
									break;
								case ItemAffinity.Water:
									if (total_aff.water >= 3) return 0x30A6; // Tidal Wave
									break;
								case ItemAffinity.Ice:
									if (total_aff.ice >= 3) return 0x309C; // Winter Storm
									break;
							}
						}
						if (total_aff.special >= 4) return 0x30AA; // Black Hole
						if (total_aff.debuff >= 3) return 0x3094; // Chaos Grenade
						return 0x3090; // Tallboy
					}

					if (total_aff.dmg >= 4) {
						if (total_aff.hasElement()) {
							switch (dom_elem) {
								case ItemAffinity.Fire:
									if (total_aff.fire >= 3) return 0x3097; // Burning Soul
									break;
								case ItemAffinity.Lightning:
									if (total_aff.lightning >= 3) return 0x30A1; // Lightning Bolt
									break;
								case ItemAffinity.Water:
									if (total_aff.water >= 3) return 0x30A6; // Tidal Wave
									break;
								case ItemAffinity.Ice:
									if (total_aff.ice >= 3) return 0x309C; // Winter Storm
									break;
							}
						}

						if (total_aff.special >= 3) return 0x30A9; // Nega Burst
						return 0x308E; // Potato Masher
					}

					if (total_aff.dmg >= 3) {
						if (total_aff.hasElement()) {
							switch (dom_elem) {
								case ItemAffinity.Fire:
									if (total_aff.fire >= 2) {
										if (total_aff.debuff >= 2) return 0x3099; // Abaddon Flame
										return 0x3096; // Firestorm
									}
									break;
								case ItemAffinity.Lightning:
									if (total_aff.lightning >= 2) {
										if (total_aff.debuff >= 2) return 0x30A3; // Thunderblast
										return 0x30A0; // Rolling Thunder
									}
									break;
								case ItemAffinity.Water:
									if (total_aff.water >= 2) {
										if (total_aff.debuff >= 2) return 0x30A8; // Dark Rain
										return 0x30A5; // Flash Flood
									}
									break;
								case ItemAffinity.Ice:
									if (total_aff.ice >= 2) {
										if (total_aff.debuff >= 2) return 0x309E; // Krysta
										return 0x309B; // Icefall
									}
									break;
							}
						}

						if (total_aff.debuff >= 3) return 0x3093; // Calamity Bomb
						if (total_aff.debuff >= 2) return 0x3092; // Hazardous Shell
						return 0x308F; // Cluster Bomb
					}

					if (total_aff.dmg >= 2) {
						if (total_aff.hasElement()) {
							switch (dom_elem) {
								case ItemAffinity.Fire:
									if (total_aff.debuff >= 2 && total_aff.fire >= 1) return 0x3098; // Brimstone
									if (total_aff.fire >= 2) return 0x3096; // Firestorm
									break;
								case ItemAffinity.Lightning:
									if (total_aff.debuff >= 2 && total_aff.lightning >= 1) return 0x30A2; // Electroshock
									if (total_aff.lightning >= 2) return 0x30A0; // Rolling Thunder
									break;
								case ItemAffinity.Water:
									if (total_aff.debuff >= 2 && total_aff.water >= 1) return 0x30A7; // Aqua Toxin
									if (total_aff.water >= 2) return 0x30A5; // Flash Flood
									break;
								case ItemAffinity.Ice:
									if (total_aff.debuff >= 2 && total_aff.ice >= 1) return 0x309D; // Black Ice
									if (total_aff.ice >= 2) return 0x309B; // Icefall
									break;
							}
						}

						if (total_aff.debuff >= 3) return 0x3093; // Calamity Bomb
						if (total_aff.debuff >= 2) return 0x3092; // Hazardous Shell
						return 0x308D; // Pineapple
					}

					switch (dom_elem) {
						case ItemAffinity.Fire:
							if (total_aff.fire >= 1) {
								if (total_aff.debuff >= 2) return 0x3098; // Brimstone
								return 0x3095; // Heat Blaster
							}
							break;
						case ItemAffinity.Lightning:
							if (total_aff.lightning >= 1) {
								if (total_aff.debuff >= 2) return 0x30A2; // Electroshock
								return 0x309F; // Thunderbolt
							}
							break;
						case ItemAffinity.Water:
							if (total_aff.water >= 1) {
								if (total_aff.debuff >= 2) return 0x30A7; // Aqua Toxin
								return 0x30A4; // Waterfall
							}
							break;
						case ItemAffinity.Ice:
							if (total_aff.ice >= 1) {
								if (total_aff.debuff >= 2) return 0x309A; // Snow Flurry
								return 0x309D; // Black Ice
							}
							break;
					}

					if (total_aff.debuff >= 2) return 0x3091; // Blaster Mine
					if (total_aff.debuff >= 1) return 0x308C; // Frag Grenade

					return 0x308B; // Grenade
				}
			}
		}

		return 0x308B; // Grenade
	}
}
