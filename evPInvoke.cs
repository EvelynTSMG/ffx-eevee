﻿using System;
using System.Runtime.InteropServices;

namespace ffx_eevee;

/* [fkelava 6/6/23 21:29]
 * P/Invokes must be declared here because of https://github.com/dotnet/runtime/issues/87188.
 * 
 * Until this is fixed, P/Invokes cannot be declared in fhcorlib and you must therefore carry your own P/Invokes.
 * 
 * Always declare your P/Invokes internal if bundling them in the hook library as such!
 */

internal static partial class evPInvoke {
#pragma warning disable SYSLIB1054 // Specifically disabled because LibraryImportAttribute cannot handle __arglist, presumably.

	[LibraryImport("kernel32.dll", EntryPoint = "GetModuleHandleW", StringMarshalling = StringMarshalling.Utf16)]
	public static partial isize GetModuleHandle(string lpModuleName);
#pragma warning restore SYSLIB1054
}
