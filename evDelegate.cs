﻿using Fahrenheit.CoreLib;
using System.Runtime.InteropServices;
using static ffx_eevee.evUtils;

namespace ffx_eevee;

public static unsafe class evDelegates {
	[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
	public delegate isize MsApUpDelegate(u32 chr_id, FhXChr* chr, u32 base_ap_add, isize param_4);

	[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
	public delegate bool MsSetRikkuLimitDelegate(u32 chr_id, FhXChr* chr, usize param_3);

	[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
	public delegate bool MsCalcEscapeDelegate(u32 chr_id);

	[UnmanagedFunctionPointer(CallingConvention.StdCall)]
	public delegate void FUN_00798b80Delegate();

	[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
	public delegate isize FUN_007996e0Delegate(u32 mon_id, u32 com_id, isize param_3);

	[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
	public delegate isize FUN_00796a00Delegate(u32 chr_id, isize param_2);

	[UnmanagedFunctionPointer(CallingConvention.StdCall)]
	public delegate u32 MsCheckRangeDelegate(u32 n, u32 min, u32 max);

	[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
	public delegate FhXChr* MsGetChrDelegate(u32 chr_id);

	[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
	public delegate isize MsSaveAddInfoDataDelegate(u32 chr_id, isize param_2);

	[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
	public delegate i32 MsSaveItemUseDelegate(u32 item_id, i32 amount);

	[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
	public delegate isize MsSetSaveCommandDelegate(u32 chr_id, u32 com_id, isize param_3);

	[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
	public delegate FhXMemCom* MsGetRomItemDelegate(u32 item_id, isize param_2);

	[UnmanagedFunctionPointer(CallingConvention.StdCall)]
	public delegate isize FUN_00860a30Delegate();

	[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
	public delegate void Sg_MenuSetReserveDelegate(isize param_1);

	[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
	public delegate void rcbgGuideUpdateExDelegate(isize param_1, isize param_2);

	[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
	public delegate u8* getDefaultChrNameDelegate(u32 chr_id);

	[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
	public delegate FhXChr* MsGetMonDelegate(u8 mon_id);

	[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
	public delegate void Sg_MainLoopDelegate(f32 param_1);

	[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
	public delegate void FUN_007c8650Delegate(isize param_1);

	[UnmanagedFunctionPointer(CallingConvention.ThisCall)]
	public delegate void showAutoSavingPromptDelegate(usize* _this);

	/*===== DAMAGE CALCULATIONS =====*/

	[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
	public delegate i32 MsCalcDamageDelegate(FhXChr* user, FhXChr* target, FhXMemCom* command,
		u8 dmg_formula, i32 power, u8 target_status__0x606, u8 targetted_stat,
		u32 should_vary, u8* used_def, u8* used_mdef, i32 base_dmg);

	[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
	public delegate i32 FUN_0078a420_multDmgWithElemDelegate(FhXChr* target, FhXMemCom* command, u32 elements, i32 base_dmg);

	[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
	public delegate void FUN_0078e680Delegate(u32 user_id, FhXChr* user, u32 target_id, FhXChr* target,
		FhXMemCom* command, u32 command_id, isize param_7, i32* damage_dealt, isize param_9, isize param_10, isize param_11);

	/*===== RNG =====*/

	[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
	public delegate u32 getChrRngIdx(u32 chr_id, u32 type);

	[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
	public delegate u32 brnd(u32 rng_seed_idx);

	/*===== ATEL =====*/

	[UnmanagedFunctionPointer(CallingConvention.StdCall)]
	public delegate void AtelJumpGameOverDelegate();

	[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
	public delegate isize AtelExecCallFuncDelegate(u32 func_selector, usize* work, usize* stack);

	[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
	public delegate void AtelResultCallFuncDelegate(u32 func_selector, usize* work, usize* storage, usize* stack_ptr);

	[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
	public delegate i32 AtelPopStackIntegerDelegate(usize* work, usize* stack_ptr);

	/*===== CALL TARGETS =====*/

	[UnmanagedFunctionPointer(CallingConvention.StdCall)]
	public delegate void CallTargetInitDelegate(usize* work, usize* storage, usize* stack);

	[UnmanagedFunctionPointer(CallingConvention.StdCall)]
	public delegate void CallTargetExecDelegate(usize* work, usize* stack);

	[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
	public delegate f32 CallTargetResultFloatDelegate(usize* work, usize* storage, usize* stack);

	[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
	public delegate i32 CallTargetResultIntDelegate(usize* work, usize* storage, usize* stack);
}