﻿using Fahrenheit.CoreLib;
using static ffx_eevee.evDelegates;
using static ffx_eevee.evFuncLib;

namespace ffx_eevee.Hooks;

public static partial class evHooks {
	public static unsafe void eeveeFUN_0078e680(u32 user_id, FhXChr* user, u32 target_id, FhXChr* target,
			FhXMemCom* command, u32 command_id, isize param_7, i32* damage_dealt,
			isize param_9, isize param_10, isize param_11) {
		if (evModule._evFUN_0078e680.GetOriginalFptrSafe(out FUN_0078e680Delegate? fptr)) {
			fptr.Invoke(user_id, user, target_id, target, command, command_id, param_7, damage_dealt, param_9, param_10, param_11);

			if (damage_dealt is not null) {
				FhLog.Log(LogLevel.Info, $" HP Damage: {*damage_dealt}");
				FhLog.Log(LogLevel.Info, $" MP Damage: {*(damage_dealt + 1)}");
				FhLog.Log(LogLevel.Info, $"CTB Damage: {*(damage_dealt + 2)}");
			}

			return;
		}

		FhLog.Log(LogLevel.Error, "Failed to call original of FUN_0078e680");
		return;
	}
}
