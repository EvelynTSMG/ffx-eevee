﻿using Fahrenheit.CoreLib;
using static ffx_eevee.evUtils;

namespace ffx_eevee.Hooks;

public static partial class evHooks {
	public static unsafe i32 eeveeMultDmgWithElem(FhXChr* target, FhXMemCom* command, u32 elements, i32 base_dmg) {
		if (elements == 0) return base_dmg;

		f64 part_dmg = base_dmg;
		u32 elem_count = countBits(elements);
		if (elem_count > 0) part_dmg /= elem_count;

		FhLog.Log(LogLevel.Info, $"Element count = {elem_count}, partial damage = {part_dmg}");

		f64 dmg = 0;

		for (u8 i = 0; i < 8; i++) {
			if ((elements >> i & 1) != 0) {
				f64 mult = 1;
				if ((target->stat_weak_e >> i & 1) != 0) mult += 0.5;
				if ((target->stat_half_e >> i & 1) != 0) mult -= 0.5;
				if ((target->stat_inv_e >> i & 1) != 0) mult = 0;
				if ((target->stat_abs_e >> i & 1) != 0) mult = -mult;

				dmg += part_dmg * mult;
			}
		}

		return (i32)dmg;
	}
}

