﻿using Fahrenheit.CoreLib;
using static ffx_eevee.evFuncLib;
using static ffx_eevee.Systems.evMixSystem;

namespace ffx_eevee.Hooks;

public static partial class evHooks {
	public static unsafe bool eeveeMsSetRikkuLimit(u32 chr_id, FhXChr* chr, usize param_3) {
		FhLog.Log(LogLevel.Info, $"MsSetRikkuLimit({chr_id}, {(usize)chr:X8}, {param_3:X8})");

		if (*(u8*)(chr + 0xDFD) != 0) {
			return true;
		}

		if (*(u8*)(chr + 0x717) != 0) {
			if (*(u8*)(chr + 0x435) == 0) {
				return false;
			}
			return true;
		}

		u16 com_id = *(u16*)(param_3 + 0x8);
		u16 item1_id = *(u16*)(param_3 + 0xA);
		u16 item2_id = *(u16*)(param_3 + 0x18);

		u16 mix_result = getMixResult(item1_id, item2_id);

		FhLog.Log(LogLevel.Info, $"Mix: {item1_id:X4} x {item2_id:X4} -> {mix_result:X4}");

		MsSaveItemUse(item1_id, -1);
		MsSaveItemUse(item2_id, -1);

		*(u8*)(param_3 + 0x3) = 1;
		*(u16*)(param_3 + 0x8) = mix_result;
		*(u16*)(param_3 + 0xA) = 0xFF;
		isize unk = FUN_007996e0(chr_id, mix_result, -1);
		*(isize*)(param_3 + 0x10) = unk;
		FUN_00796a00(chr_id, unk);

		if (unk != 0) {
			MsSetSaveCommand(chr_id, mix_result, 1);
		}

		*(u8*)(chr + 0x717) = 1;
		return item1_id == 0;
	}
}