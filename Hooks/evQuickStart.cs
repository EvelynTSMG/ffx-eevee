﻿using Fahrenheit.CoreLib;
using System.Runtime.InteropServices;
using static ffx_eevee.evDelegates;
using static ffx_eevee.evFuncLib;
using static ffx_eevee.evUtils;

namespace ffx_eevee.Hooks;

public static partial class evHooks {
	private static bool tried_quick_reset = false;

	public static unsafe void eeveeQuickReset(f32 param_1) {
		if (evModule._evQuickReset.GetOriginalFptrSafe(out Sg_MainLoopDelegate? fptr)) {
			fptr.Invoke(param_1);
		}

		// L1 + R1 + L2 + R2 + Select
		if (*FhXGlobals.pad_input == 0x010F) {
			if (!tried_quick_reset) {
				// No movie playing
				if (FhXGlobals.save_data->on_memory_movie != 0x0) FhXGlobals.save_data->on_memory_movie = 0x0;

				if (FhXGlobals.save_data->on_memory_movie == 0x0) {
					AtelJumpGameOver();
					FhLog.Log(LogLevel.Info, "Performed quick reset");
					return;
				}

				FhLog.Log(LogLevel.Info, $"Can't quick reset while movie is playing, movie state: {FhXGlobals.save_data->on_memory_movie}");
			}

			tried_quick_reset = true;
		} else {
			tried_quick_reset = false;
		}
	}

	public static unsafe void eeveeSkipAutoSavePrompt(usize* _this) {
		return;
	}
}
