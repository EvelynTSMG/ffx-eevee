﻿using Fahrenheit.CoreLib;
using static ffx_eevee.evFuncLib;
using static ffx_eevee.evUtils;

namespace ffx_eevee.Hooks;

public static partial class evHooks {
    public static unsafe isize eeveeMsApUp(u32 chr_id, FhXChr* chr, u32 base_ap_add, isize param_4) {
        u32 new_ap;
        isize uVar1;

        if (chr->stat_escape_flag) {
            return param_4;
        }

        if (chr->has_no_ap) {
            new_ap = 0;
        } else {
            new_ap = base_ap_add;
        }

        if (chr->stat_inbattle != 0 && chr->stat_death == 0) {
            uVar1 = 2;

            if (!chr->has_aa30)  {
                uVar1 = param_4;
            }
            param_4 = uVar1;

            if (chr->has_triple_ap) {
                new_ap *= 3;
            }  else if (chr->has_double_ap) {
                new_ap *= 2;
            }
        }

        u32 old_ap = FhXGlobals.btl_data->get_ap_temp[chr_id];

        new_ap = MsCheckRange(old_ap + new_ap, 0, 999999999);

        FhXGlobals.btl_data->get_ap_temp[chr_id] = new_ap;

        FhLog.Log(LogLevel.Info, $"Raise AP of {chrIdToName(chr_id)} from {old_ap} to {new_ap} (by {new_ap - old_ap})");

        return param_4;
    }
}