﻿using Fahrenheit.CoreLib;
using System;
using static ffx_eevee.evFuncLib;
using static ffx_eevee.evUtils;

namespace ffx_eevee.Hooks;

public static partial class evHooks {
	public static unsafe i32 eeveeMsCalcDamage(FhXChr* user, FhXChr* target, FhXMemCom* command,
			u8 dmg_formula, i32 power, u8 target_status__0x606, u8 targetted_stat,
			u32 _should_vary, u8* ref_used_def, u8* ref_used_mdef, i32 base_dmg) {
		bool should_vary = _should_vary != 0;

		u32 chr_rng_idx = getChrRngIdx(user->chr_id, 0);
		u8 target_def = target->stat_vit;
		u8 target_mdef = target->stat_spirit;
		i32 current_stat = 0;
		u32 max_stat = 0;
		u32 variance = 256; // Percentage is variance / 256
		f64 dmg = base_dmg;

		const u8 STR_VS_DEF = 0x01;
		const u8 STR_IGNORE_DEF = 0x02;
		const u8 MAG_VS_MDEF = 0x03;
		const u8 MAG_IGNORE_MDEF = 0x04;
		const u8 CURRENT_STAT = 0x05;
		const u8 MULTIPLE_50 = 0x06;
		const u8 HEALING = 0x07;
		const u8 MAX_STAT = 0x08;
		const u8 MULTIPLE_50_VARIANCE = 0x09;
		const u8 TARGET_MAX_MP = 0x0A;
		const u8 TARGET_MAX_CTB = 0x0B;
		const u8 TARGET_MP = 0x0C;
		const u8 TARGET_CTB = 0x0D;
		const u8 MAG_SPECIAL = 0x0F;
		const u8 USER_MAX_HP = 0x10;
		const u8 CELESTIAL_HIGH_HP = 0x11;
		const u8 CELESTIAL_HIGH_MP = 0x12;
		const u8 CELESTIAL_LOW_HP = 0x13;
		const u8 GIL = 0x15;
		const u8 TARGET_KILLS = 0x16;
		const u8 MULTIPLE_9999 = 0x17;

		const i32 ZOMBIE = 0x2;
		const i32 ARMOR_BREAK = 0x40;
		const i32 MENTAL_BREAK = 0x80;

		const i32 IS_HEALING = 0x10;

		const u8 TARGETTING_HP = 0x1;
		const u8 TARGETTING_MP = 0x2;
		const u8 TARGETTING_CTB = 0x3;

		switch (targetted_stat) {
			case TARGETTING_HP: {
				current_stat = target->current_hp;
				max_stat = target->stat_maxhp;
				break;
			}
			case TARGETTING_MP: {
				current_stat = target->current_mp;
				max_stat = target->stat_maxmp;
				break;
			}
			case TARGETTING_CTB: {
				current_stat = target->current_ctb;
				max_stat = target->stat_max_ctb;
				break;
			}
		}

		if ((command is not null) && command->damages_hp) {
			if ((target_status__0x606 & ARMOR_BREAK) != 0) {
				target_def = 0;
			}

			if ((target_status__0x606 & MENTAL_BREAK) != 0) {
				target_mdef = 0;
			}
		}

		if (should_vary) {
			u32 variance_rng = brnd(chr_rng_idx);
			variance = (variance_rng % 32) + 240; // Between 240 and 271 (93.75% - 105.86%)
		}

		f64 temp_dmg;
		f64 celestial_factor;
		f64 power_factor = power / 16d;
		f64 variance_factor = variance / 256d;

		f64 str_base(f64 x) => Math.Pow(x, 3) / 32 + 30;

		f64 mag_base(f64 x) => Math.Pow(x, 2) / 6 + power;

		f64 defense_factor(f64 defense, f64 def_stat_buff) {
			f64 buff_factor = (15 - def_stat_buff) / 15;
			f64 def_factor = 730 - (defense * 51 - Math.Pow(defense, 2) / 11) / 10;
			def_factor /= 730;
			def_factor *= buff_factor;
			return def_factor;
		}

		switch (dmg_formula) {
			case STR_VS_DEF:
				temp_dmg = user->cheer_count + user->stat_str;
				temp_dmg = str_base(temp_dmg);
				temp_dmg *= defense_factor(target_def, target->cheer_count);
				temp_dmg *= power_factor;
				dmg = temp_dmg * variance_factor;
				break;

			case STR_IGNORE_DEF:
				temp_dmg = user->cheer_count + user->stat_str;
				temp_dmg = str_base(temp_dmg);
				temp_dmg *= power_factor;
				dmg = temp_dmg * variance_factor;
				break;

			case MAG_VS_MDEF:
				temp_dmg = user->focus_count + user->stat_mag;
				temp_dmg = mag_base(temp_dmg);
				temp_dmg *= defense_factor(target_mdef, target->focus_count);
				temp_dmg *= power_factor * 4;
				dmg = temp_dmg * variance_factor;
				break;

			case MAG_IGNORE_MDEF:
				temp_dmg = user->focus_count + user->stat_mag;
				temp_dmg = mag_base(temp_dmg);
				temp_dmg *= power_factor * 4;
				dmg = temp_dmg * variance_factor;
				break;

			case CURRENT_STAT:
				dmg = current_stat * power_factor;
				break;

			case MULTIPLE_50:
				dmg = power * 50;
				break;

			case HEALING:
				temp_dmg = user->stat_mag + user->focus_count + power;
				temp_dmg *= power_factor * 8;
				dmg = temp_dmg * variance_factor;
				break;

			case MAX_STAT:
				dmg = max_stat * power_factor;
				break;

			case MULTIPLE_50_VARIANCE:
				temp_dmg = power * 50;
				dmg = temp_dmg * variance_factor;
				break;

			case TARGET_MAX_MP:
				dmg = target->stat_maxmp * power_factor;
				break;

			case TARGET_MAX_CTB:
				dmg = target->stat_max_ctb * power_factor;
				break;

			case TARGET_MP:
				dmg = target->current_mp * power_factor;
				break;

			case TARGET_CTB:
				dmg = target->current_ctb * power_factor;
				break;

			case 0xe:
				temp_dmg = str_base(user->stat_str);
				return (i32)(temp_dmg * power_factor);

			case MAG_SPECIAL:
				temp_dmg = user->focus_count + user->stat_mag;
				temp_dmg = str_base(temp_dmg);
				temp_dmg *= power_factor;
				dmg = temp_dmg * variance_factor;
				break;

			case USER_MAX_HP:
				// Vanilla is / 10, this is now / 16
				dmg = user->stat_maxhp * power_factor;
				break;

			case CELESTIAL_HIGH_HP:
				celestial_factor = (user->stat_hp * 100 / user->stat_maxhp + 10) / 110;
				temp_dmg = user->cheer_count + user->stat_str;
				temp_dmg = celestial_factor * str_base(temp_dmg);
				temp_dmg *= power_factor;
				dmg = temp_dmg * variance_factor;
				break;

			case CELESTIAL_HIGH_MP:
				celestial_factor = (user->stat_mp * 100 / user->stat_maxmp + 10) / 110;
				temp_dmg = user->cheer_count + user->stat_str;
				temp_dmg = celestial_factor * str_base(temp_dmg);
				temp_dmg *= power_factor;
				dmg = temp_dmg * variance_factor;
				break;

			case CELESTIAL_LOW_HP:
				celestial_factor = (130 - user->stat_hp * 100 / user->stat_maxhp) / 60;
				temp_dmg = user->cheer_count + user->stat_str;
				temp_dmg = celestial_factor * str_base(temp_dmg);
				temp_dmg *= power_factor;
				dmg = temp_dmg * variance_factor;
				break;

			case 0x14:
				temp_dmg = str_base(user->stat_mag);
				return (i32)(temp_dmg * power_factor);

			case GIL:
				dmg = FhXGlobals.btl->chosen_gil / 10;
				break;

			case TARGET_KILLS:
				// if target is player
				if (target->chr_id < 0x12) {
					FhXPlySave* ply_save = (FhXPlySave*)&FhXGlobals.save_data->ply_arr[target->chr_id * sizeof(FhXPlySave)];

					dmg = ply_save->ply_enemies_defeated * power;
				} else {
					// Not vanilla
					// Get number of dead characters in the dumbest way possible
					i32 count = 0;
					f64 dead_multiplier = 1;

					u8[] frontline = new u8[3];
					for (u32 i = 0; i <3; i++) {
						frontline[i] = *(FhXGlobals.save_data->party_order + i);
					}

					for (u32 i = 0; i <3; i++) {
						FhXChr* chr = MsGetChr(frontline[i]);
						if (chr->stat_death_status != 0) {
							count++;
						}
					}
					if (count == 1) dead_multiplier = 1.5;
					if (count >= 2) dead_multiplier = 2;

					temp_dmg = user->cheer_count + user->stat_str;
					temp_dmg = str_base(temp_dmg);
					temp_dmg *= defense_factor(target_def, target->cheer_count);
					temp_dmg *= power_factor;
					temp_dmg *= dead_multiplier;
					dmg = temp_dmg * variance_factor;
				}

				break;

			case MULTIPLE_9999:
				dmg = power * 9999;
				break;
		}

		// Command is healing and target isn't Zombied
		if (command is not null
			&& ((command->dmg_props & IS_HEALING) != 0)
			&& (target_status__0x606 & ZOMBIE) == 0) {
			dmg = -dmg;
		}

		if (ref_used_def is not null) {
			*ref_used_def = target_def;
		}

		if (ref_used_mdef is not null) {
			*ref_used_mdef = target_mdef;
		}

		string dmg_class = "";
		switch (command->dmg_class) {
			case 1: dmg_class = " HP"; break;
			case 2: dmg_class = " MP"; break;
			case 4: dmg_class = " CTB"; break;
		}

		FhLog.Log(LogLevel.Info, $"Calculated {(i32)dmg}{dmg_class} {(dmg >= 0 ? "damage" : "healing")} (variance {variance_factor * 100:00.00}%) for {chrIdToName(user->chr_id)} vs {chrIdToName(target->chr_id)}");

		return (i32)dmg;
	}
}