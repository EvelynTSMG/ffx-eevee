﻿using Fahrenheit.CoreLib;
using System.Linq;
using System.Threading.Tasks;
using static ffx_eevee.evDelegates;

namespace ffx_eevee.Hooks;

public static partial class evHooks {
	public static unsafe isize eeveeCSR(u32 func_selector, usize* work, usize* stack) {
		/*uint[] no_log = { 0x0000, 0x005F, 0x0044, 0x007D, 0x00FB };
		//uint[] block = { 0x0000, 0x00D5, 0x601A, 0x400D };
		uint[] block = { 0x400D, 0x601A, 0x6038, 0x6062, 0x6063, 0x6064, 0x607B, 0x607C, 0x8004, 0x8005 };
		if (!no_log.ToList().Contains((uint)func_selector) && !block.ToList().Contains((uint)func_selector)) FhLog.Log(LogLevel.Info, $"eeveeCSR {func_selector:X4}");
		if (block.ToList().Contains((uint)func_selector)) {
			FhLog.Log(LogLevel.Info, $"Prevented {func_selector:X4}");
			return 5;
		}*/

		if (evModule._evCSR.GetOriginalFptrSafe(out AtelExecCallFuncDelegate? fptr)) {
			return fptr.Invoke(func_selector, work, stack);
		}

		return 5;
	}

	public static unsafe void eeveeAtelResultCallFunc(u32 func_selector, usize* work, usize* storage, usize* stack_ptr) {
		if (func_selector == 0x01B2) {
			FhLog.Log(LogLevel.Info, $"Getting amount of {*(stack_ptr+1):X4}s");
		}

		if (evModule._evOwo.GetOriginalFptrSafe(out AtelResultCallFuncDelegate? fptr)) {
			fptr.Invoke(func_selector, work, storage, stack_ptr);
		}

		if (func_selector == 0x01B2) {
			FhLog.Log(LogLevel.Info, $"Amount is {*(stack_ptr+1)}");
		}

		return;
	}
}
